package ar.com.rockcodes.noobprotector;


import org.bukkit.configuration.file.FileConfiguration;

public class Settings {

	
	public static String execute_command_new = "";
	public static String execute_command_player_new = "";
	public static boolean no_pvp_new = true;
	public static int no_pvp_horas = 24;
	
    public static void load(){
		FileConfiguration noobconf = NoobProtector.plugin.getConfig();
		Settings.execute_command_new = noobconf.getString("execute_command_new","");
		Settings.execute_command_player_new = noobconf.getString("execute_command_player_new","");
		Settings.no_pvp_new = noobconf.getBoolean("no_pvp_new",true);
		Settings.no_pvp_horas = noobconf.getInt("no_pvp_new",24);
	}
    
    public static void reload(){
    	NoobProtector.plugin.reloadConfig();
    	load();
    }
}
