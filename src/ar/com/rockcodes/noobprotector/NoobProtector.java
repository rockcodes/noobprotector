package ar.com.rockcodes.noobprotector;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import ar.com.rockcodes.noobprotector.listener.PlayerListener;

public class NoobProtector extends JavaPlugin {

	public static NoobProtector plugin ; 
	public static NoobList nooblist;
	@Override
	public void onDisable() {
		NoobProtector.nooblist.save();
	}

	@Override
	public void onEnable() {
		NoobProtector.plugin = this;
		this.saveDefaultConfig();
		Settings.load();
	
		NoobProtector.nooblist = new NoobList();
		this.getServer().getPluginManager().registerEvents(new PlayerListener(), this);
		if (!VaultHelper.setupEconomy()) {
		    getLogger().warning("Could not set up economy! - Running without an economy.");
		}
		if (!VaultHelper.setupPermissions()) {
		    getLogger().severe("Cannot link with Vault for permissions! Disabling plugin!");
		    getServer().getPluginManager().disablePlugin(this);
		    return;
		}
		this.getCommand("noob").setExecutor(new Comandos());
		this.getCommand("pvp").setExecutor(new ComandosPvp());

		
	}

	public static YamlConfiguration loadconfig(String filename){
		final File challengesfile = new File(NoobProtector.plugin.getDataFolder(), filename);
		YamlConfiguration chconfig = new YamlConfiguration();
		if (!challengesfile.exists()) {
			try {
				chconfig.save(challengesfile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			
			try {
				chconfig.load(challengesfile);
			
			} catch (IOException | InvalidConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return chconfig;
	}
}
