package ar.com.rockcodes.noobprotector;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class Comandos implements CommandExecutor {

	
	@Override
	public boolean onCommand(CommandSender sender, Command command,String label, String[] args) {
		
		if (sender instanceof Player) {
			Player player = (Player)sender;
			if(args.length==0){command_help((Player) sender,args); return true;}
			if(VaultHelper.permission.has(player, "noobprotector.commands")){
				
				switch(args[0]){
				case "info": command_info(player,args); return true;	
				}
				
			}else{
				
				player.sendMessage(ChatColor.RED+"No tienes permisos para esto!");
			}
			
			
			
			
        } 
		return true;
       

	}

	private void command_help(Player player, String[] args) {
		player.sendMessage("/noob info <player> <info>");
	}

	private void command_info(Player player, String[] args) {
		if(args.length<3) {
			player.sendMessage("argumentos insuficientes usa /noob info <player> <info>");
			return;
		}
		
		Player destplayer = NoobProtector.plugin.getServer().getPlayer(args[1]);
		if(destplayer==null)return;
		
		NoobProtector.plugin.getServer().dispatchCommand(destplayer, "inform "+args[2]);
		
		
	}
}
