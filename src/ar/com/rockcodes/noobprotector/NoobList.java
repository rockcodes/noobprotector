package ar.com.rockcodes.noobprotector;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.configuration.file.YamlConfiguration;



public class NoobList {

	Map<String,Long> noobs;
	
	public Map<String, Long> getNoobs() {
		return noobs;
	}

	public void setNoobs(Map<String, Long> noobs) {
		this.noobs = noobs;
	}

	public NoobList(){
		this.noobs = new HashMap<String,Long>();
		this.load();
	}
	
    public void load(){
		YamlConfiguration noobconf = NoobProtector.loadconfig("Noobs.yml");
		for(String key : noobconf.getKeys(false)){
			this.noobs.put(key, noobconf.getLong(key));
			
		}
		
	}
    
    public void save(){
    	
    	YamlConfiguration noobconf = new YamlConfiguration();
    	for(Entry<String,Long> entry : this.noobs.entrySet()){
    		noobconf.set(entry.getKey(), entry.getValue());
    	}
    	try {
			noobconf.save(new File(NoobProtector.plugin.getDataFolder(), "Noobs.yml"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    

	
}
