package ar.com.rockcodes.noobprotector.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import ar.com.rockcodes.noobprotector.NoobProtector;
import ar.com.rockcodes.noobprotector.Settings;
import net.md_5.bungee.api.ChatColor;

public class PlayerListener implements Listener{

	
	@EventHandler
	public void PlayerLogin(PlayerJoinEvent event) {
		
		Player p = event.getPlayer();
		if(!p.hasPlayedBefore()) {
			NoobProtector.plugin.getLogger().info("Player "+p.getName()+" jugador nuevo.");
			if(Settings.no_pvp_new)
			NoobProtector.nooblist.getNoobs().put(p.getName(), System.currentTimeMillis()+Settings.no_pvp_horas*3600000L);
		}else{
			
			NoobProtector.plugin.getLogger().info("Player "+p.getName()+"ya jugo antes");
			if(!NoobProtector.nooblist.getNoobs().containsKey(p.getName()))return;
			
			if(System.currentTimeMillis()>NoobProtector.nooblist.getNoobs().get(p.getName())){
				NoobProtector.plugin.getLogger().info("Pvp activado para "+p.getName()+" ");
				NoobProtector.nooblist.getNoobs().remove(p.getName());
			}
			
		}
		
	}
	
	
	@EventHandler
	public void PlayerJoin(PlayerJoinEvent event) {
		
		Player p = event.getPlayer();
		
		
		if(!p.hasPlayedBefore()) {
			
			if(!Settings.execute_command_new.equals("")){
				String cmd = Settings.execute_command_new.replace("&p", p.getName());
				NoobProtector.plugin.getServer().dispatchCommand(Bukkit.getConsoleSender(), cmd);
			}
			
			if(!Settings.execute_command_player_new.equals("")){
				NoobProtector.plugin.getServer().dispatchCommand(p, Settings.execute_command_player_new);
			}		
			
		}
		
	}
	
	@EventHandler
	public void onAttack(EntityDamageByEntityEvent event) {
		Player attacker  , defender;
		if( !(event.getEntity() instanceof Player))return;
		defender = (Player)event.getEntity();
		
		if(event.getDamager() instanceof Projectile ){
			
			Projectile proy = (Projectile) event.getDamager();
			if(proy.getShooter() instanceof Player){
				 attacker = (Player)event.getDamager();
				
			}else return;
		}else if(event.getDamager() instanceof Player){
			attacker = (Player)event.getDamager();
			
		}else return;

		
		if(NoobProtector.nooblist.getNoobs().containsKey(attacker.getName())){
			event.setCancelled(true);
			attacker.sendMessage(ChatColor.RED+"Tu PVP esta desactivado hasta 24hs , eres invulnerable vs jugadores hasta ese entonces. Para activarlo usa /pvp activar");
		}
		else if(NoobProtector.nooblist.getNoobs().containsKey(defender.getName())){
			event.setCancelled(true);
			attacker.sendMessage(ChatColor.RED+"No puedes aprovecharte de los nuevos jugadores! abusivo!");
		}
		
			
		
	}

}
